import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isAdmin:boolean =true;
  upperText:string = 'Display uppercase';
  lowerText:string = 'Display uppercase';   
  percentValue: number = 0.5;
  date:Date = new Date();
  money: number = 598;
  profile : number =2;
  user:  User ={
      name: 'Bob',
      age: 25
    }

}

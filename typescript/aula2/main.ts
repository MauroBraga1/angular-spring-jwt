import { Student } from './student';
import {Person} from './person';

let p = new Person("Mauro");

p.showAge(30);

let s = new Student('Jonh');
s.showAge(22);
import { DaoInterface } from './daointerface';

export class PersonDao implements DaoInterface{
    tableName:string;
    insert(object:any):boolean{
        console.log("Inserindo pessoa");
        return true;
    }
    update(object:any):boolean{
        return true;
    }
    delete(id:number):boolean{
        return true;
    }
    find(id:number):any{
        return null;
    }
    findAll():[any]{
        return [""];
    }

}
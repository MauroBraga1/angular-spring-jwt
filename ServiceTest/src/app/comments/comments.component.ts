import { CommentService } from './comment.service';
import { Comment } from './comment.modelo';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  commets$ : Observable<Comment[]>;

  constructor(private commentsService: CommentService ) { }

  ngOnInit() {
    this.commets$ = this.commentsService.getComments();
  }

}
